require 'sidekiq/web'

Rails.application.routes.draw do
  resources :report1, only: [:index]
  resources :report2, only: [:index]

  mount Sidekiq::Web => '/sidekiq'
  root :to => 'report1#index'
end
