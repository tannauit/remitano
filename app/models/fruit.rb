class Fruit < ApplicationRecord
  belongs_to :seed

  validates :name, presence: true
end
