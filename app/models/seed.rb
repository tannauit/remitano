class Seed < ApplicationRecord
  has_one :fruit

  validates :label, presence: true
end
