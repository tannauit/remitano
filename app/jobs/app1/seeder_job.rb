class App1::SeederJob < ApplicationJob
  queue_as :default

  DELAY = 2.minutes

  def perform()
    jobs = Sidekiq::ScheduledSet.new.select { |job|
      job.display_class == self.class.name
    }
    self.class.set(wait: DELAY).perform_later if jobs.count == 0

    Seed.create!(label: "1-#{rand}")
  end
end
