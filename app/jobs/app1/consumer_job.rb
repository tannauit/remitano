class App1::ConsumerJob < ApplicationJob
  queue_as :default

  DELAY = 5.minutes

  def perform()
    jobs = Sidekiq::ScheduledSet.new.select { |job|
      job.display_class == self.class.name
    }
    self.class.set(wait: DELAY).perform_later if jobs.count == 0

    return unless seed = Seed.where(is_consumed: false).where("label LIKE '1-%'").sample(1).first

    Fruit.create!(name: "f-#{rand}", seed_id: seed.id)
    seed.update_attributes(is_consumed: true)
  end
end
