class App2::SeederJob < ApplicationJob
  queue_as :default

  DELAY = 1.minutes

  def perform()
    jobs = Sidekiq::ScheduledSet.new.select { |job|
      job.display_class == self.class.name
    }
    self.class.set(wait: DELAY).perform_later if jobs.count == 0

    Seed.create!(label: "2-#{rand}")
  end
end
