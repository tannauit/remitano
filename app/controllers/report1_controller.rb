class Report1Controller < ApplicationController
  def index
    @latest = Seed.includes(:fruit).where("label LIKE '1-%'").order(id: :desc).limit(10)
  end
end
