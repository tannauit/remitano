class Report2Controller < ApplicationController
  def index
    @latest = Seed.includes(:fruit).where("label LIKE '2-%'").order(id: :desc).limit(10)
  end
end
