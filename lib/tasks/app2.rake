namespace :app2 do
  task :seeder => :environment do
    App2::SeederJob.perform_later
  end

  task :consumer => :environment do
    App2::ConsumerJob.perform_later
  end
end
