namespace :app1 do
  task :seeder => :environment do
    App1::SeederJob.perform_later
  end

  task :consumer => :environment do
    App1::ConsumerJob.perform_later
  end
end
